HOSTNAME=$1
KEY=~/.ssh/nicole-key-aws.pem 

if [ -z $HOSTNAME ]
then
    read "you need to add an IP address"
fi

ssh -o StrictHostKeyChecking=no -i $KEY ec2-user@$HOSTNAME '
# update mariadb to install on your machine
sudo yum update -y
# install mariadb
sudo yum install mariadb-server -y
# start mariadb on your machine
sudo systemctl start mariadb
# check status of the download
sudo systemctl status mariadb
# enable mariadb on the machine
sudo systemctl enable mariadb

# go and get the instructions from this site
sudo yum install wget
wget https://raw.githubusercontent.com/cwoodruff/ChinookDatabase/master/Scripts/Chinook_MySql.sql

mysql -u root --password="" -e "source Chinook_MySql.sql"
mysql -u root --password="" -e "use Chinook; show tables;"
'
